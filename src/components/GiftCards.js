import walmart from "../images/walmart.jpg";
// import apple from "../images/apple.jpg";
import googleplay from "../images/Googleplay.jpg";

const GiftCards = () => {
  return (
    <div className="gift-cards ">
      <div className="card-body    amazon">
        <div className="line-1">
          <div className="amazon-text">
            <h2>Amazon.com</h2>
            <div className="giftCon flex ">
              <i className="mr-5 fa fa-long-arrow-right"></i>

              <h4>Gift Card</h4>
            </div>
          </div>
          <span className="char">
            <i className="fa-brands fa-amazon text-white"></i>
          </span>
        </div>

        <div className="line-2">
          <i className=" fa-brands fa-amazon text-white"></i>
        </div>
      </div>
      <div className="card-body    itunes">
        <p>
          Apple Store <br /> &itunes
        </p>

        {/* <i className="fa-brands fa-apple"></i> */}

        <div className="line-2">
          <i className="fa-brands fa-apple"></i>
        </div>
      </div>

      <div className="card-body    google">
        <div className="googleplay ">
          <img className="googlePlayMage" src={googleplay} alt="google logo" />
          <p>Google Play</p>
        </div>

        <div className="line-2 google-line2">
          <img className="googlePlayMage" src={googleplay} alt="google logo" />
        </div>
      </div>

      <div className="card-body  ebay">
        <div className="line-2 ">
          <p>
            <span className="text-red-500">e</span>
            <span className="text-blue-500">b</span>
            <span className="text-yellow-300">a</span>
            <span className="text-green-400">y</span>
          </p>
        </div>
      </div>

      <div className="card-body  walmart">
        <div className="line-2  ">
          <div className="flex items-center walmart-text">
            <p className="text-white text-2xl mr-1">Walmart</p>
            <img className="walmartLogo" src={walmart} alt="walmart" />
          </div>
        </div>
      </div>
    </div>
  );
};
export default GiftCards;
